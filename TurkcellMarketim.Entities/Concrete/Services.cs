﻿using System;
using System.Collections.Generic;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class Services : IEntity
    {
        public Services()
        {
            ServiceLogs = new HashSet<ServiceLogs>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public bool? IsAuthenticationRequest { get; set; }

        public ICollection<ServiceLogs> ServiceLogs { get; set; }
    }
}
