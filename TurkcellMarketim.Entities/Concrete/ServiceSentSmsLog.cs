﻿using System;
using System.Collections.Generic;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class ServiceSentSmsLog : IEntity
    {
        public int MessageId { get; set; }
        public string SendSms { get; set; }
        public string Msisdn { get; set; }
        public DateTime? SentTime { get; set; }
        public int? ServiceLogId { get; set; }

        public ServiceLogs ServiceLog { get; set; }
    }
}
