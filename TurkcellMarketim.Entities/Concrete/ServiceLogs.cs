﻿using System;
using System.Collections.Generic;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class ServiceLogs : IEntity
    {
        public ServiceLogs()
        {
            OpTurkcellMessages = new HashSet<OpTurkcellMessages>();
            ServiceProvisionLog = new HashSet<ServiceProvisionLog>();
            ServiceSentSmsLog = new HashSet<ServiceSentSmsLog>();
        }

        public int ServiceLogId { get; set; }
        public int? ServiceId { get; set; }
        public DateTime? RequestTime { get; set; }
        public int? LogTypeId { get; set; }
        public string Description { get; set; }
        public string Msisdn { get; set; }
        public bool? IsSuccessful { get; set; }

        public LogTypes LogType { get; set; }
        public Services Service { get; set; }
        public ICollection<OpTurkcellMessages> OpTurkcellMessages { get; set; }
        public ICollection<ServiceProvisionLog> ServiceProvisionLog { get; set; }
        public ICollection<ServiceSentSmsLog> ServiceSentSmsLog { get; set; }
    }
}