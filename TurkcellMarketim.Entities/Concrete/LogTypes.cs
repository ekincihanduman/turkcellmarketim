﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class LogTypes : IEntity
    {
        public LogTypes()
        {
            ServiceLogs = new HashSet<ServiceLogs>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Required]
        public int LogTypeId { get; set; }
        [Required]
        [StringLength(100)]
        public string LogDescription { get; set; }

        public ICollection<ServiceLogs> ServiceLogs { get; set; }
    }
}
