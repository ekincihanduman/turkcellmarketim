﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.Core.Entities;

namespace TurkcellMarketim.Entities.Concrete
{
    public class Item :IEntity
    {
        public int Id { get; set; }
        public int? ErrorId { get; set; }
        public string ErrorDetail { get; set; }
        public string NotificationId { get; set; }
        public string Type { get; set; }
        public string UserText { get; set; }
        public int? OrderLineItemId { get; set; }
        public OrderLineItem OrderLineItem { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
