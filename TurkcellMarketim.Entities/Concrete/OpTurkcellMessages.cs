﻿using TurkcellMarketim.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace TurkcellMarketim.Entities.Concrete
{
    public partial class OpTurkcellMessages : IEntity
    {
        public OpTurkcellMessages()
        {
            OpTurkcellMessageParameters = new HashSet<OpTurkcellMessageParameters>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Required]
        public int Id{ get; set; }
        public string SeasonId { get; set; }
        public string MessageType { get; set; }
        public string ReceiverMsIsdn { get; set; }
        public string MpStringContent { get; set; }
        public string Channel { get; set; }
        public string LargeAccount { get; set; }
        public string VariantId { get; set; }
        public DateTime? RecordDate { get; set; }
        public int? ServiceLogId { get; set; }

        public ServiceLogs ServiceLog { get; set; }
        public ICollection<OpTurkcellMessageParameters> OpTurkcellMessageParameters { get; set; }
    }
}