﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TurkcellMarketim.Entities.Shared.Controllers
{
    public enum ResultCode
    {
        [Display(Name = "İşlem Başarılı")]
        Success = 200,
        [Display(Name = "Eksik Parametre")]
        ModelIsNotValid = -23001,
        [Display(Name = "Beklenmedik Bir Hata Oluştu")]
        AnUnexpectedErrorHasOccurred = -23102,
    }
}
