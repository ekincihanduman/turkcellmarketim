﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurkcellMarketim.Entities.Shared.Models
{
    public class APIResult
    {
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public object Content { get; set; }
    }
}
