﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurkcellMarketim.Entities.Shared.Models
{
    public abstract class ModelBase
    {
        public virtual Guid RowGuid { get;}
        public virtual DateTime DateTime{ get; }
    }
}
