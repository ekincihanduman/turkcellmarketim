﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TurkcellMarketim.Entities.Shared.Models
{
    public class LoggingEvents
    {
        public const int GenerateItems = 1000;
        public const int ListItems = 1001;
        public const int GetItem = 1002;
        public const int InsertItem = 1003;
        public const int UpdateItem = 1004;
        public const int DeleteItem = 1005;
        public const int Login = 1006;
        public const int Token = 1007;
        public const int TokenIsValid = 1008;
        public const int CouldVerify = 1009;
        
        public const int GetItemNotFound = 4000;
        public const int UpdateItemNotFound = 4001;
        public const int TokenIsNotValid = 4002;
        public const int CouldNotVerify = 4003;
    }
}
