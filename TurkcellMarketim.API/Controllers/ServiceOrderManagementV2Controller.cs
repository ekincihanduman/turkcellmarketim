﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Turkcell.Service.OrderManagementV2;
using TurkcellMarketim.API.Models;
using TurkcellMarketim.Business.EF;
using TurkcellMarketim.Business.EF.Abstract;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;
using TurkcellMarketim.Entities.Concrete;
using TurkcellMarketim.Entities.Shared.Controllers;
using TurkcellMarketim.Entities.Shared.Models;

namespace TurkcellMarketim.API.Controllers
{
    //[Produces("application/json")]
    [Route("api/[controller]")]
    //[Authorize]
    public class ServiceOrderManagementV2Controller : Controller
    {
        private readonly ILogTypes _efLogTypes;
        public ServiceOrderManagementV2Controller(ILogTypes efLogTypes)
        {
            _efLogTypes = efLogTypes;
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<APIResult> Order([FromBody]string csd)
        {
            TurkcellOperator op = new TurkcellOperator();

            return new APIResult
            {
                Content = null,
                ResultCode = (int)ResultCode.Success,
                ResultMessage = ""
            };
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<APIResult> CreateOrder(CreateOrderRequest request)
        {
            TurkcellOperator op = new TurkcellOperator();
            await op.OrderManagemenV2(request);

            return new APIResult
            {
                Content = null,
                ResultCode = (int)ResultCode.Success,
                ResultMessage = ""
            };
        }
    }
}