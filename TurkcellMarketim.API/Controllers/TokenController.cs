﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using TurkcellMarketim.Entities.Shared.Controllers;
using TurkcellMarketim.Entities.Shared.Models;
using TurkcellMarketim.Entities.Shared.Models.JWT;

namespace TurkcellMarketim.API.Controllers
{
    [Produces("application/json")]
    //[Produces("aapplication/x-www-form-urlencoded")]
    
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class TokenController : Controller // BaseAPIContreller<LoginTokenModel, APIResult>
    {
        ILogger _logger;
        public TokenController(ILogger<TokenController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        public APIResult Post([FromBody] LoginTokenModel model)
        {
            if (model.Username == "Marketim" && model.Password == "Angel4you!")
            {
                _logger.LogInformation(LoggingEvents.CouldVerify, "Token username & password is verify", model);
                var claims = new[]
                {
                    new Claim(ClaimTypes.Name, model.Username)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Kyros-Turkcell-Marketim")); //_configuration["SecurityKey"]
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "Marketim.Securtiy.Brearer",
                    audience: "Marketim.Securtiy.Brearer",
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);

                _logger.LogInformation(LoggingEvents.Token, "Token was created", model);

                return new  APIResult
                {
                    ResultCode = (int)ResultCode.Success,
                    ResultMessage = ResultCode.Success.GetName(),
                    Content = new JwtSecurityTokenHandler().WriteToken(token)
                };
            }
            _logger.LogInformation(LoggingEvents.CouldNotVerify, "Token username & password is not verify", model);
            return new APIResult
            {
                ResultCode = (int)ResultCode.ModelIsNotValid,
                ResultMessage = ResultCode.ModelIsNotValid.GetName(),
                Content = null
            };
        }
    }
}