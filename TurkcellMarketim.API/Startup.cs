﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TurkcellMarketim.Business.EF;
using TurkcellMarketim.Business.EF.Abstract;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;
using TurkcellMarketim.Entities.Shared.Models.JWT;

namespace TurkcellMarketim.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddTransient<ILogTypes, EfLogTypes>();
            services.AddTransient<IOpTurkcellMessageParameters, EfOpTurkcellMessageParameters>();
            services.AddTransient<IOpTurkcellMessages, EfOpTurkcellMessages>();
            services.AddTransient<IServiceProvisionLog, EfServiceProvisionLog>();
            services.AddTransient<IServiceLogs, EfServiceLogs>();
            services.AddTransient<IServices, EfServices>();
            services.AddTransient<IServiceSentSmsLog, EfServiceSentSmsLog>();
            services.AddTransient<IOrder, EfOrders>();
            services.AddTransient<IOrderLines, EfOrderLines>();
            services.AddTransient<IOrderLineItems, EfOrderLineItems>();
            services.AddTransient<IBusinessInteraction, EfBusinessInteraction>();
            services.AddTransient<IItems, EfItems>();
            services.AddMvc();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).
            AddJwtBearer(options =>
            {
                options.TokenValidationParameters =
                new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "Marketim.Securtiy.Brearer",
                    ValidAudience = "Marketim.Securtiy.Brearer",
                    IssuerSigningKey = JwtSecurityKey.Create("Kyros-Turkcell-Marketim")
                };
                options.Events = new JwtBearerEvents();
            });

            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory )
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Token}/{action=Post}");
            });
            app.UseStaticFiles();
        }
    }
}
