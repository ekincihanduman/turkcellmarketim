﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.API.Models
{
    public class SmsLogger
    {
        private static EfServiceSentSmsLog _smsLogger;
        private static SmsLogger _smsLogg;
        private SmsLogger(EfServiceSentSmsLog smsLogger)
        {
            _smsLogger = smsLogger;
        }
        public static SmsLogger SmsLoggs
        {
            get { return _smsLogg; }
            set { _smsLogg = value; }
        }

        public int Log(string msisdn, string sendSms, int serviceLogId)
        {
            ServiceSentSmsLog serviceSentSmsLog = new ServiceSentSmsLog
            {
                Msisdn = msisdn,
                SendSms = sendSms,
                SentTime = DateTime.UtcNow,
                ServiceLogId = serviceLogId
            };
            _smsLogger.Add(serviceSentSmsLog);
            return serviceSentSmsLog.MessageId;
        }
    }
}