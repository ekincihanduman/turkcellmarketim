﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using TurkcellMarketim.Entities.Concrete;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;

namespace TurkcellMarketim.API.Models
{
    public class ServiceLogger
    {
        private static EfServiceLogs _serviceLog;
        private static ServiceLogger _sLog;
        private ServiceLogger()
        {

        }
        public static ServiceLogger SLog
        {
            get { return _sLog; }
            set { _sLog = value; }
        }

        private ServiceLogger(EfServiceLogs serviceLog)
        {
            _serviceLog = serviceLog;
        }

        public int Log(ServiceLogs serviceLogs)
        {
            ServiceLogs serviceLog = new ServiceLogs
            {
                ServiceId = serviceLogs.ServiceId,
                LogTypeId = serviceLogs.LogTypeId,
                Description = serviceLogs.Description,
                Msisdn = serviceLogs.Msisdn,
                IsSuccessful = serviceLogs.IsSuccessful,
                RequestTime = DateTime.UtcNow
            };

            _serviceLog.Add(serviceLog);
            return serviceLog.ServiceLogId;
        }
    }
}