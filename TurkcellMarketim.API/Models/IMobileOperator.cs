﻿using System.Threading.Tasks;
using Turkcell.Service.OrderManagementV2;

namespace TurkcellMarketim.API.Models
{
    public interface IMobileOperator
    {
        void SendSms(string msisdn, string smsBody);
        Task<ServiceOrderManagementResponse> OrderManagemenV2(CreateOrderRequest createOrderRequest);
    }
}