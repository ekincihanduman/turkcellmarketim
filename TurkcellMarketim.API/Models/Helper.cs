﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Globalization;

namespace TurkcellMarketim.API.Models
{
    public class Helper
    {
        public static bool ControlSmsError(Turkcell.Service.SendMessage.SendSMSOutput o)
        {
            if (o.TSOresult.statusCode == "0")
                return true;
            return false;
        }
        public static bool ControlMsisdnMessage(string msisdn, string message)
        {
            if (!ControlMsisdn(msisdn) || !ControlIsEmpty(message))
                return false;
            return true;
        }
        public static bool ControlMsisdn(string msisdn)
        {
            if (msisdn.Length < 13 || IsNumeric(msisdn))
                return true;
            return false;
        }
        public static bool ControlIsEmpty(string message)
        {
            if (message.Length != 0)
                return true;
            return false;
        }
        public static bool IsNumeric(string input)
        {
            int test;
            return int.TryParse(input, out test);
        }

        public static bool IsDecimal(string controlDecimal)
        {
            decimal decValue;
            if (decimal.TryParse(controlDecimal, out decValue))
                return true;
            else
                return false;
        }
        public static ServiceResponse SendSMSOutputCreator(string errorCode, string errorDescription, string statusCode)
        {
            return new ServiceResponse { errorCode = errorCode, errorDescription = errorDescription, statusCode = statusCode };
        }
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();
            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);
            string gonder = Convert.ToBase64String(byteHash);
            return Convert.ToBase64String(byteHash);
        }
    }
}