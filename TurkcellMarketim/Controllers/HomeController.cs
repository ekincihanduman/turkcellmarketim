﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;
using TurkcellMarketim.Entities.Concrete;
using TurkcellMarketim.Helpers;
using TurkcellMarketim.Web.Models;
using TurkcellMarketim.Web.ViewModels.HomeViewModels;
using Microsoft.EntityFrameworkCore;

namespace TurkcellMarketim.Controllers
{
    public class HomeController : Controller
    {
        private MarketimDBContext marketimDBContext = new MarketimDBContext();
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;

        public HomeController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ILogger<HomeController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }
        
        [HttpGet]
        public IActionResult Index()
        {

            DateTime rightNow = DateTime.UtcNow.AddMinutes(-1);

            SendSmsReportViewModel sendSmsReportViewModel = new SendSmsReportViewModel
            {
                serviceSentSmsLogList = marketimDBContext.ServiceSentSmsLog.Include(x => x.ServiceLog).Where(x => x.SentTime > rightNow).OrderByDescending(x => x.SentTime).ToList()
            };

            return View(model: sendSmsReportViewModel);
            if (true)
            {
             
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult SendSMS(string smsBody)
        {
            try
            {
                string smsBodyContent = smsBody;

                if (Helper.ControlIsEmpty(smsBodyContent))
                {
                    IMobileOperator mOperator = MobileOperatorFactory.GetInstance("Turkcell");
                    var activeUsers = marketimDBContext.ServiceProvisionLog.Where(x => x.Action == "ACTIVATE").OrderByDescending(p => p.ProvisionTime).ToLookup(x => x.Msisdn);
                    int chunkSize = 20;
                    int i = 0;

                    var splittedLists = activeUsers.GroupBy(s => i++ / chunkSize).Select(g => g.ToArray()).ToList();

                    foreach (var selectedList in splittedLists)
                    {
                        int selectedListCount = selectedList.Count();

                        for (int k = 0; k < selectedListCount; k++)
                        {
                            string selectedMsisdn = selectedList[k].FirstOrDefault().Msisdn;
                            if (Helper.ControlMsisdn(selectedMsisdn))
                            {
                                mOperator.SendSms(selectedMsisdn, smsBody);
                                Console.WriteLine(selectedMsisdn + " " + smsBody);
                            }
                            else
                            {
                                Console.WriteLine("Sending error :" + selectedMsisdn + " " + smsBody);
                            }
                        }
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            catch (Exception)
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}