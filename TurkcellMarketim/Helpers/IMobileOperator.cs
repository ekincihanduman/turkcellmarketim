﻿namespace TurkcellMarketim.Helpers
{
    public interface IMobileOperator
    {
        void SendSms(string msisdn, string smsBody);
    }
}