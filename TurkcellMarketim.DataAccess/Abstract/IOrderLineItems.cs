﻿using TurkcellMarketim.Core.DataAccess;
using TurkcellMarketim.Entities.Concrete;

namespace TurkcellMarketim.DataAccess.Abstract
{
    public interface IOrderLineItems: IEntityRepository<OrderLineItem>
    {
    }
}
