﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TurkcellMarketim.DataAccess.Migrations
{
    public partial class ServiceOrderManagementv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_BusinessInteractions_BusinessInteractionId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderLineItems_OrderLines_OrderLineId",
                table: "OrderLineItems");

            migrationBuilder.DropTable(
                name: "BusinessInteractions");

            migrationBuilder.DropColumn(
                name: "BusinessInteractionId",
                table: "OrderLineItems");

            migrationBuilder.DropColumn(
                name: "ErrorDetail",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ErrorId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "NotificationId",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "SystemWillRetry",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "UserText",
                table: "Items",
                newName: "ItemValue");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "Items",
                newName: "ItemKey");

            migrationBuilder.RenameColumn(
                name: "BusinessInteractionId",
                table: "Items",
                newName: "OrderLineItemId");

            migrationBuilder.RenameIndex(
                name: "IX_Items_BusinessInteractionId",
                table: "Items",
                newName: "IX_Items_OrderLineItemId");

            migrationBuilder.AddColumn<int>(
                name: "ServiceLogId",
                table: "OrderLines",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrderLineId",
                table: "OrderLineItems",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_ServiceLogId",
                table: "OrderLines",
                column: "ServiceLogId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items",
                column: "OrderLineItemId",
                principalTable: "OrderLineItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLineItems_OrderLines_OrderLineId",
                table: "OrderLineItems",
                column: "OrderLineId",
                principalTable: "OrderLines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLines_ServiceLogs_ServiceLogId",
                table: "OrderLines",
                column: "ServiceLogId",
                principalTable: "ServiceLogs",
                principalColumn: "ServiceLogId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_OrderLineItems_OrderLineItemId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderLineItems_OrderLines_OrderLineId",
                table: "OrderLineItems");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderLines_ServiceLogs_ServiceLogId",
                table: "OrderLines");

            migrationBuilder.DropIndex(
                name: "IX_OrderLines_ServiceLogId",
                table: "OrderLines");

            migrationBuilder.DropColumn(
                name: "ServiceLogId",
                table: "OrderLines");

            migrationBuilder.RenameColumn(
                name: "OrderLineItemId",
                table: "Items",
                newName: "BusinessInteractionId");

            migrationBuilder.RenameColumn(
                name: "ItemValue",
                table: "Items",
                newName: "UserText");

            migrationBuilder.RenameColumn(
                name: "ItemKey",
                table: "Items",
                newName: "Type");

            migrationBuilder.RenameIndex(
                name: "IX_Items_OrderLineItemId",
                table: "Items",
                newName: "IX_Items_BusinessInteractionId");

            migrationBuilder.AlterColumn<int>(
                name: "OrderLineId",
                table: "OrderLineItems",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BusinessInteractionId",
                table: "OrderLineItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ErrorDetail",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ErrorId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NotificationId",
                table: "Items",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SystemWillRetry",
                table: "Items",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "BusinessInteractions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessInteractions", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Items_BusinessInteractions_BusinessInteractionId",
                table: "Items",
                column: "BusinessInteractionId",
                principalTable: "BusinessInteractions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLineItems_OrderLines_OrderLineId",
                table: "OrderLineItems",
                column: "OrderLineId",
                principalTable: "OrderLines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
