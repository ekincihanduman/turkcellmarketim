﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TurkcellMarketim.DataAccess.Migrations
{
    public partial class CreateOrderResponse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LargeAccount",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "VariantId",
                table: "Orders",
                newName: "OrderCode");

            migrationBuilder.RenameColumn(
                name: "ProductIdField",
                table: "OrderLines",
                newName: "ProductId");

            migrationBuilder.RenameColumn(
                name: "LineOfferIdField",
                table: "OrderLines",
                newName: "LineOfferId");

            migrationBuilder.RenameColumn(
                name: "IdentifierForLineOfferIdField",
                table: "OrderLines",
                newName: "IdentifierForLineOfferId");

            migrationBuilder.RenameColumn(
                name: "ProductIdField",
                table: "OrderLineItems",
                newName: "ProductId");

            migrationBuilder.RenameColumn(
                name: "OfferIdField",
                table: "OrderLineItems",
                newName: "OfferId");

            migrationBuilder.RenameColumn(
                name: "ContinueField",
                table: "OrderLineItems",
                newName: "Continue");

            migrationBuilder.RenameColumn(
                name: "ActionField",
                table: "OrderLineItems",
                newName: "Action");

            migrationBuilder.AddColumn<int>(
                name: "BusinessInteractionId",
                table: "OrderLineItems",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BusinessInteractions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessInteractions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BusinessInteractionId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ErrorDetail = table.Column<string>(nullable: true),
                    ErrorId = table.Column<int>(nullable: true),
                    NotificationId = table.Column<int>(nullable: true),
                    SystemWillRetry = table.Column<bool>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    UserText = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_BusinessInteractions_BusinessInteractionId",
                        column: x => x.BusinessInteractionId,
                        principalTable: "BusinessInteractions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_BusinessInteractionId",
                table: "Items",
                column: "BusinessInteractionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "BusinessInteractions");

            migrationBuilder.DropColumn(
                name: "BusinessInteractionId",
                table: "OrderLineItems");

            migrationBuilder.RenameColumn(
                name: "OrderCode",
                table: "Orders",
                newName: "VariantId");

            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "OrderLines",
                newName: "ProductIdField");

            migrationBuilder.RenameColumn(
                name: "LineOfferId",
                table: "OrderLines",
                newName: "LineOfferIdField");

            migrationBuilder.RenameColumn(
                name: "IdentifierForLineOfferId",
                table: "OrderLines",
                newName: "IdentifierForLineOfferIdField");

            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "OrderLineItems",
                newName: "ProductIdField");

            migrationBuilder.RenameColumn(
                name: "OfferId",
                table: "OrderLineItems",
                newName: "OfferIdField");

            migrationBuilder.RenameColumn(
                name: "Continue",
                table: "OrderLineItems",
                newName: "ContinueField");

            migrationBuilder.RenameColumn(
                name: "Action",
                table: "OrderLineItems",
                newName: "ActionField");

            migrationBuilder.AddColumn<string>(
                name: "LargeAccount",
                table: "Orders",
                nullable: true);
        }
    }
}
