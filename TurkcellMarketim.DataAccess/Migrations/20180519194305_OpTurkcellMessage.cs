﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TurkcellMarketim.DataAccess.Migrations
{
    public partial class OpTurkcellMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MessageId",
                table: "OpTurkcellMessages",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ParameterId",
                table: "OpTurkcellMessageParameters",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "OpTurkcellMessages",
                newName: "MessageId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "OpTurkcellMessageParameters",
                newName: "ParameterId");
        }
    }
}
