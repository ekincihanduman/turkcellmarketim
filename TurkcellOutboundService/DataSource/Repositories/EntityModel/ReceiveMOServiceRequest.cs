﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Configuration;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace TurkcellOutboundService.DataSource.Repositories.EntityModel
{
    public class ReceiveMOServiceRequest
    {
        public MESSAGELIST MESSAGE_LIST { get; set; }
    }
    public class MESSAGELIST
    {
        public List<MESSAGE> MESSAGE { get; set; }
    }
    public class MESSAGE
    {
        public string CHANNEL { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string RECEIVER_MSISDN { get; set; }
        public string VARIANT_ID { get; set; }
        public string LARGE_ACCOUNT { get; set; }
        public string MP_STRING_CONTENT { get; set; }
            
        public PARAMETERS PARAMETERS { get; set; }
            
    }
    public class PARAMETERS
    {
        private string[] _content;
        [System.Xml.Serialization.XmlElementAttribute("content")]
        public string[] Content
        {
            get { return _content; }
            set { _content = value; }
        }
    }
    [XmlRoot(Namespace = NS1)]
    public class WebServiceResult
    {
        private XmlSerializerNamespaces _namespaces;
        private const string NS1 = "http://tempuri.org/NS1";
        private const string NS2 = "http://tempuri.org/NS2";
        public const string NS3 = "http://tempuri.org/NS3";

        [XmlElement(Namespace = NS2)]
        public InnerElementType InnerElement { get; set; }

        public XmlElement Element { get; set; }

        [XmlNamespaceDeclarations]
        public XmlSerializerNamespaces Namespaces
        {
            get
            {
                if (_namespaces == null)
                {
                    _namespaces = new XmlSerializerNamespaces(
                        new[]
                        {
                        new XmlQualifiedName("pfx1", NS1),
                        new XmlQualifiedName("pfx2", NS2),
                        new XmlQualifiedName("pfx3", NS3),
                        });
                }
                return _namespaces;
            }
            set { _namespaces = value; }
        }
    }

    public class InnerElementType
    {
        [XmlAttribute(Namespace = WebServiceResult.NS3)]
        public string Attribute { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}