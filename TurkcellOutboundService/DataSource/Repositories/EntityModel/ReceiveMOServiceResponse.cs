﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TurkcellOutboundService.DataSource.Repositories.EntityModel
{
    public class ReceiveMOServiceResponse
    {
        public TSOresult TSOresult { get; set; }
    }
    public class TSOresult
    {
        public string statusCode { get; set; }
        public string errorCode { get; set; }
        public string errorDescription { get; set; }
    }
}