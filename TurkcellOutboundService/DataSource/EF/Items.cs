//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TurkcellOutboundService.DataSource.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Items
    {
        public int Id { get; set; }
        public Nullable<int> OrderLineItemId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Type { get; set; }
        public string UserText { get; set; }
        public string ErrorDetail { get; set; }
        public Nullable<int> ErrorId { get; set; }
        public string NotificationId { get; set; }
    
        public virtual OrderLineItems OrderLineItems { get; set; }
    }
}
