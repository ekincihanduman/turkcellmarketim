﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using TurkcellOutboundService.DataSource.EF;
using TurkcellOutboundService.DataSource.Repositories;
using TurkcellOutboundService.PartnerServiceProvisioning;
using TurkcellOutboundService.Sources;

namespace Turkcellmarketim.ProvisionService
{
    [WebService(Namespace = "http://www.turkcell.com/spgw/PartnerServiceProvisioning", Description = "PartnerServiceProvisioning", Name = "PartnerServiceProvisioning")]
    [WebServiceBinding(Name = "PartnerServiceProvisioning_Binding", Namespace = "http://www.turkcell.com/spgw/PartnerServiceProvisioning",
        ConformsTo = WsiProfiles.BasicProfile1_1)]
    [SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.SoapAction, Use = SoapBindingUse.Literal)]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    //[WebServiceBindingAttribute(ConformsTo = WsiProfiles.BasicProfile1_1, Name = "partnerServiceProvisioning")]
    [System.ComponentModel.ToolboxItem(false)]
    public class Provisioning : PartnerServiceProvisioning
    {
        //public static ServiceLogger ServiceLogger = ServiceLogger.sLog();
        [SoapDocumentMethod(Action = "http://sdp.turkcell.com.tr/services/action/PartnerServiceProvisioning/partnerServiceProvisioning",
           RequestElementName = "PartnerServiceProvisioningRequest", RequestNamespace = "http://www.turkcell.com/spgw/partnerserviceprovisioning/PartnerServiceProvisioningTypes",
           ResponseElementName = "PartnerServiceProvisioningResponse", ResponseNamespace = "http://partnerprovisioningservice.turkcell.services.ws.fourplay.com.tr",
           Use = SoapBindingUse.Literal,
           ParameterStyle = SoapParameterStyle.Bare,
           Binding = "PartnerServiceProvisioning_Binding")]
       // [FaultContractAttribute(
       //typeof(PartnerServiceProvisioningRequest), Name = "fault1",
       //ProtectionLevel = ProtectionLevel.EncryptAndSign)]

        //[return: XmlElement("PartnerServiceProvisioningResponse")]
        //[return: MessageParameter(Name = "PartnerServiceProvisioning")]
        public partnerServiceProvisioningResponse1 partnerServiceProvisioning(partnerServiceProvisioningRequest1 request)
        {
            return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = new PartnerServiceProvisioningResponse { statusCode = "0" } };
            PartnerServiceProvisioningResponse PartnerServiceProvisioningResponse = new PartnerServiceProvisioningResponse();
            PartnerServiceProvisioningRequest partnerServiceProvisioningRequest = request.PartnerServiceProvisioningRequest;
            ApplicationRepositroy<ServiceProvisionLog> _serviceProvisionLog = new ApplicationRepositroy<ServiceProvisionLog>();

            try
            {
                ServiceLogger ServiceLogger = ServiceLogger.sLog();
                ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, partnerServiceProvisioningRequest.action + " start", partnerServiceProvisioningRequest.msisdn, true);
                //Servis sonucu, 0 : başarılı, 1: hatalı

                if (partnerServiceProvisioningRequest != null)
                {
                    var provisionModel = _serviceProvisionLog.GetList(x => x.Msisdn == partnerServiceProvisioningRequest.msisdn).FirstOrDefault();

                    ServiceProvisionLog Model = provisionModel ?? new ServiceProvisionLog();

                    Model.Action = partnerServiceProvisioningRequest.action ?? "";
                    Model.Msisdn = partnerServiceProvisioningRequest.msisdn ?? "";
                    Model.OfferId = partnerServiceProvisioningRequest.offerId ?? "";
                    Model.ProductId = partnerServiceProvisioningRequest.productId ?? "";
                    Model.ServiceVariantId = partnerServiceProvisioningRequest.serviceVariantId ?? "";
                    Model.TransactionId = partnerServiceProvisioningRequest.transactionId ?? "";
                    Model.ProvisionTime = DateTime.UtcNow;

                    if (provisionModel != null)
                        _serviceProvisionLog.Update(Model);
                    else
                        _serviceProvisionLog.Add(Model);

                    var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, partnerServiceProvisioningRequest.action, partnerServiceProvisioningRequest.msisdn, true);

                    Model.ServiceLogId = ServiceLogId;

                    _serviceProvisionLog.Update(Model);

                    PartnerServiceProvisioningResponse.statusCode = "0";

                    return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
                }

                PartnerServiceProvisioningResponse.statusCode = "1";
                PartnerServiceProvisioningResponse.errorCode = "900";
                PartnerServiceProvisioningResponse.errorDescription = "PartnerServiceProvisioningRequest is null.";

                return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
            }
            catch (SoapException ex)
            {
                //var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Error, ex.Message, partnerServiceProvisioningRequest.msisdn, false);

                //Servis sonucu, 0 : başarılı, 1: hatalı
                PartnerServiceProvisioningResponse response = new PartnerServiceProvisioningResponse
                {
                    statusCode = "1",
                    errorCode = "901",
                    errorDescription = "PartnerServiceProvisioningRequest failed. "
                };
                return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
            }
        }
        public async Task<partnerServiceProvisioningResponse1> partnerServiceProvisioningAsync(partnerServiceProvisioningRequest1 request)
        {
            PartnerServiceProvisioningResponse PartnerServiceProvisioningResponse = new PartnerServiceProvisioningResponse();
            PartnerServiceProvisioningRequest partnerServiceProvisioningRequest = request.PartnerServiceProvisioningRequest;
            ApplicationRepositroy<ServiceProvisionLog> _serviceProvisionLog = new ApplicationRepositroy<ServiceProvisionLog>();
            try
            {
                //ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, partnerServiceProvisioningRequest.action + " start", partnerServiceProvisioningRequest.msisdn, true);
                //Servis sonucu, 0 : başarılı, 1: hatalı

                if (partnerServiceProvisioningRequest != null)
                {
                    var provisionModel = _serviceProvisionLog.GetList(x => x.Msisdn == partnerServiceProvisioningRequest.msisdn).FirstOrDefault();

                    ServiceProvisionLog Model = provisionModel ?? new ServiceProvisionLog();

                    Model.Action = partnerServiceProvisioningRequest.action ?? "";
                    Model.Msisdn = partnerServiceProvisioningRequest.msisdn ?? "";
                    Model.OfferId = partnerServiceProvisioningRequest.offerId ?? "";
                    Model.ProductId = partnerServiceProvisioningRequest.productId ?? "";
                    Model.ServiceVariantId = partnerServiceProvisioningRequest.serviceVariantId ?? "";
                    Model.TransactionId = partnerServiceProvisioningRequest.transactionId ?? "";
                    Model.ProvisionTime = DateTime.UtcNow;

                    if (provisionModel != null)
                        await _serviceProvisionLog.UpdateAsync(Model);
                    else
                        await _serviceProvisionLog.AddAsync(Model);

                    //var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, partnerServiceProvisioningRequest.action, partnerServiceProvisioningRequest.msisdn, true);

                    //Model.ServiceLogId = ServiceLogId;

                    await _serviceProvisionLog.UpdateAsync(Model);

                    PartnerServiceProvisioningResponse.statusCode = "0";

                    return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
                }

                PartnerServiceProvisioningResponse.statusCode = "1";
                PartnerServiceProvisioningResponse.errorCode = "900";
                PartnerServiceProvisioningResponse.errorDescription = "PartnerServiceProvisioningRequest is null.";

                return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
            }
            catch (SoapException ex)
            {
                //var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Error, ex.Message, partnerServiceProvisioningRequest.msisdn, false);

                //Servis sonucu, 0 : başarılı, 1: hatalı
                PartnerServiceProvisioningResponse response = new PartnerServiceProvisioningResponse
                {
                    statusCode = "1",
                    errorCode = "901",
                    errorDescription = "PartnerServiceProvisioningRequest failed. "
                };
                return new partnerServiceProvisioningResponse1 { PartnerServiceProvisioningResponse = PartnerServiceProvisioningResponse };
            }
        }
    }
}
