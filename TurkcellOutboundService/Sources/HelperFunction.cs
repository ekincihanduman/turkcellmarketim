﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TurkcellOutboundService.DataSource.Repositories.EntityModel;

namespace TurkcellOutboundService.Sources.AuthRepository
{
    public class HelperFunction
    {
        #region GetValueFromObject
        public static T GetValueFromObject<T>(object Value)
        {
            if (Value == null || Value == DBNull.Value)
            {
                return default(T);
            }

            try
            {
                return (T)GetValueFromObject(typeof(T), Value);
            }

            catch (Exception)
            {
                return default(T);
            }
        }
        public static string GetAPIEndPoint()
        {
            return ConfigurationManager.AppSettings["ApiUrl"] + "/api/" ?? "";
        }

        public static bool ContainMessageText(MESSAGE message, out int action)
        {
            action = 0;
            if (GetMessageText("JoinTM").Where(x => x == message.PARAMETERS.Content.First().ToUpper()).Count() > 0)
            {
                action = (int)OrderAction.CREATE;
                return true;
            }
            else if (GetMessageText("CancelTM").Where(x => x == message.PARAMETERS.Content.First().ToUpper()).Count() > 0)
            {
                action = (int)OrderAction.TERMCANCELLATION; 
                return true;
            }
            return false;
        }
        public static T GetValueFromObject<T>(object Value, T DefaultValue)
        {
            if (Value == null || Value == DBNull.Value)
            {
                return DefaultValue;
            }

            try
            {
                return (T)GetValueFromObject(typeof(T), Value);
            }

            catch (Exception)
            {
                return default(T);
            }
        }

        private static object GetValueFromObject(Type type, object value)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(type);

            if (converter == null)
            {
                return null;
            }

            try
            {
                object result = null;
                if (type == typeof(bool) || type == typeof(bool?))
                {
                    int? x = null;
                    try
                    {
                        x = Convert.ToInt32(value);
                    }

                    catch (Exception)
                    {

                    }

                    if (x.HasValue)
                    {
                        result = Convert.ToBoolean(x);
                    }

                    else
                    {
                        result = Convert.ToBoolean(value);
                    }
                }

                else
                {
                    result = converter.ConvertFromString(value.ToString());
                }

                return result;
            }

            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region Hashing

        public static string CreateOneWayHash(string inValue, HashAlgorithmTypeEnum HashAlgorithm = HashAlgorithmTypeEnum.SHA1)
        {
            byte[] result = new byte[inValue.Length];

            try
            {
                HashAlgorithm hash = null;
                switch (HashAlgorithm)
                {
                    case HashAlgorithmTypeEnum.SHA1:
                        hash = new SHA1CryptoServiceProvider();
                        break;
                    case HashAlgorithmTypeEnum.MD5:
                        hash = new MD5CryptoServiceProvider();
                        break;
                }

                result = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(inValue));
                return Convert.ToBase64String(result);
            }

            catch (CryptographicException ce)
            {
                throw new Exception(ce.Message);
            }
        }
        #endregion

        #region CreateKey
        public static string CreateKey(int Length)
        {
            string Key = Guid.NewGuid().ToString().Replace("-", "").Substring(0, Length).ToUpper();
            return Key;
        }
        #endregion

        public static string GetValueFromHtml(string Html, string IdTagName)
        {
            Match rg = Regex.Match(Html, "=\"" + IdTagName + "\">(.*?)</td>");
            string result = rg.Groups[1].ToString();
            return result;
        }

        public static bool IsBasicType(Type Type)
        {
            return Type.IsValueType || Type.Equals(typeof(string));
        }

        public static string GetServiceParameter(IDictionary<string, string> parameters)
        {
            if (parameters.Count == 0)
                return "";

            StringBuilder sb = new StringBuilder();
            int flag = 1;
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append($"{parameter.Key}={parameter.Value}");
                if (flag < parameters.Count)
                    sb.Append("&");

                flag++;
            }
            return sb.ToString();
        }
        public static DateTime ParseDateTime(string Input)
        {
            DateTime result;

            try
            {
                result = DateTime.Parse(Input, new CultureInfo("en-US").DateTimeFormat);
            }

            catch (Exception)
            {
                try
                {
                    result = DateTime.Parse(Input, Thread.CurrentThread.CurrentCulture.DateTimeFormat);
                }

                catch (Exception)
                {
                    try
                    {
                        result = DateTime.Parse(Input, CultureInfo.InvariantCulture.DateTimeFormat);
                    }

                    catch (Exception)
                    {
                        result = DateTime.Parse(Input, new CultureInfo("tr-TR"));
                    }
                }
            }

            return result;
        }

        public static string ConvertDateTimeToTRString(DateTime DateTime)
        {
            return DateTime.ToString("dd.MM.yyyy");
        }
        public static string GetOfferId()
        {
            return ConfigurationManager.AppSettings["OfferId"] ?? "";
        }
        public static string GetCampaignId()
        {
            return ConfigurationManager.AppSettings["CampaignId"] ?? "";
        }
        public static IEnumerable<string> GetMessageText(string key)
        {
            return ConfigurationManager.AppSettings[key].Split(';').ToList();
        }
    }

    public enum OrderAction
    {
        CREATE = 1,
        TERMCANCELLATION = 4,
        PARAMETERUPDATE = 11
    }
    public enum HashAlgorithmTypeEnum
    {
        SHA1,
        MD5
    }
    public enum ChannelType
    {
        SMS = 17,
        ThirdPart = 23
    }
    public enum RequestMethod
    {
        [Display(Name = "POST")]
        POST = 1,
        [Display(Name = "GET")]
        GET = 2,
        [Display(Name = "PUT")]
        PUT = 3,
        [Display(Name = "DELETE")]
        DELETE = 4,
        [Display(Name = "LOCK")]
        LOCK = 5,
        [Display(Name = "PATCH")]
        PATH = 6,
        [Display(Name = "COPY")]
        COPY = 7,
        [Display(Name = "HEAD")]
        HEAD = 8,
        [Display(Name = "OPTIONS")]
        OPTIONS = 9,
        [Display(Name = "UNLOCK")]
        UNLOCK = 10
    }
}
