﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Configuration;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace TurkcellOutboundService.Sources
{
    public class SID : SoapHeader
    {
        public SID()
        {   
            MustUnderstand = false;
        }

        [XmlElement(ElementName = "sessionId")]
        public string sessionId { get; set; }
        //[XmlElement(ElementName = "transaction-list")]
        //public TRANSACTION transactionlist { get; set; }

    }
    public class TRANSACTION
    {
        private string[] _content;
        [System.Xml.Serialization.XmlElementAttribute("transaction-id")]
        public string[] Content
        {
            get { return _content; }
            set { _content = value; }
        }
    }
    [XmlRoot(Namespace = "http://schemas.xmlsoap.org/ws/2004/08/addressing")]
    public class Header : SoapHeader
    {
    }
}