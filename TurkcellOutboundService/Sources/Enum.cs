public enum ServiceType
{
    Authentication = 1,
    SendSMS = 2,
    Provision = 3,
    ReceiveMO = 4
}

public enum LogTypes
{
    Ok = 1,
    Error = 2
}