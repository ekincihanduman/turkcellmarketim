﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TurkcellOutboundService.DataSource.EF;
using TurkcellOutboundService.DataSource.Repositories;

namespace TurkcellOutboundService.Sources
{
    public class ServiceLogger
    {
        private static ServiceLogger serviceLogger = new ServiceLogger();
        private static TurkcellMarketimDBEntities dc = new TurkcellMarketimDBEntities();
        private readonly ApplicationRepositroy<ServiceLogs> _dc;
        private ServiceLogger()
        {
            _dc = _dc ?? new ApplicationRepositroy<ServiceLogs>();
        }
        public static ServiceLogger sLog() { return serviceLogger; }

        public int Log(int serviceId, int logTypeId, string description, string msisdn, bool isSuccessfull)
        {

            ServiceLogs serviceLog = new ServiceLogs
            {
                ServiceId = serviceId,
                LogTypeId = logTypeId,
                Description = description,
                Msisdn = msisdn,
                IsSuccessful = isSuccessfull,
                RequestTime = DateTime.Now
            };

            _dc.Add(serviceLog);
            return serviceLog.ServiceLogId;
        }
    }
}