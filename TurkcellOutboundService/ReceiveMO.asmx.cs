﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using TurkcellOutboundService.DataSource.EF;
using TurkcellOutboundService.DataSource.Repositories;
using TurkcellOutboundService.DataSource.Repositories.EntityModel;
using TurkcellOutboundService.ServiceOrderManagementV2;
using TurkcellOutboundService.Sources;
using TurkcellOutboundService.Sources.AuthRepository;

namespace Turkcellmarketim.ProvisionService
{
    /// <summary>
    /// Summary description for ReceiveMO
    /// </summary>
    [WebService(Namespace = "http://sdp.turkcell.com.tr/mapping/generated", Name = "ReceiveMessageBinding")]
    [WebServiceBinding(Name = "ReceiveMessageBinding", Namespace = "http://sdp.turkcell.com.tr/mapping/generated",
        ConformsTo = WsiProfiles.BasicProfile1_1)]
    [SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.SoapAction, Use = SoapBindingUse.Literal)]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [WebServiceBindingAttribute(ConformsTo = WsiProfiles.BasicProfile1_1, Name ="ReceiveMOMessage")]
    [System.ComponentModel.ToolboxItem(false)]
    public class ReceiveMO : WebService // ProviderBase<OpTurkcellMessages>         
    {
        public static ServiceLogger ServiceLogger = ServiceLogger.sLog();
        private ApplicationRepositroy<OpTurkcellMessages> _opTurkcellMessages = new ApplicationRepositroy<OpTurkcellMessages>();
        private ApplicationRepositroy<OpTurkcellMessageParameters> _opTurkcellMessageParameters = new ApplicationRepositroy<OpTurkcellMessageParameters>();
        private TSOresult _tSOresult = new TSOresult();
        [XmlElement(Form = XmlSchemaForm.Unqualified)]

        public SID sid;
       
        public ReceiveMO()
        {
            ServiceLogger = ServiceLogger.sLog();
        }
        [WebMethod()]
        [SoapDocumentMethod(RequestElementName = "ReceiveMOMessageInput", Binding = "ReceiveMessageBinding", Action = "http://sdp.turkcell.com.tr/services/action/ReceiveMessage/ReceiveMOMessage")]
        [SoapHeader("sid", Direction = SoapHeaderDirection.In)]
        public ReceiveMOServiceResponse ReceiveMOMessage(List<MESSAGE> MESSAGE_LIST)
        {
            try
            {
                //Servis sonucu, 0 : başarılı, 1: hatalı
                OpTurkcellMessages opTurkcellMessages;
                int ServiceLogId = 0;

                List<MESSAGE> messageList = MESSAGE_LIST;

                if (messageList != null)
                {
                    foreach (MESSAGE message in messageList)
                    {
                        opTurkcellMessages = new OpTurkcellMessages();
                        opTurkcellMessages.SeasonId = sid.sessionId.ToString();
                        opTurkcellMessages.VariantId = message.VARIANT_ID;
                        opTurkcellMessages.MessageType = message.MESSAGE_TYPE;
                        opTurkcellMessages.Channel = message.CHANNEL;
                        opTurkcellMessages.LargeAccount = message.LARGE_ACCOUNT;
                        opTurkcellMessages.ReceiverMsIsdn = message.RECEIVER_MSISDN;
                        opTurkcellMessages.RecordDate = DateTime.UtcNow;
                        opTurkcellMessages.MpStringContent = message.MP_STRING_CONTENT;
                        
                        if (HelperFunction.ContainMessageText(message, out int serviceAction))
                        {

                            opTurkcellMessages.ServiceLogId = ServiceLogger.Log((int)ServiceType.ReceiveMO, (int)LogTypes.Ok, message.MP_STRING_CONTENT, message.RECEIVER_MSISDN, true);
                            
                            opTurkcellMessages = _opTurkcellMessages.Add(opTurkcellMessages);
                            if (message.PARAMETERS.Content.Count() > 0)
                            {
                                foreach (var item in message.PARAMETERS.Content)
                                {
                                    if (item.ToString().Length > 0)
                                    {
                                        OpTurkcellMessageParameters opTurkcellMessageParameter = new OpTurkcellMessageParameters();

                                        opTurkcellMessageParameter.ParameterContent = item.ToString();
                                        opTurkcellMessageParameter.OpTurkcellMessageId = opTurkcellMessages.Id;
                                        _opTurkcellMessageParameters.Add(opTurkcellMessageParameter);
                                        //_opTurkcellMessageParameters.Add(opTurkcellMessageParameter);
                                    }
                                }
                            }

                            APIService service = new APIService();
                            service.ServiceUrl = $" { HelperFunction.GetAPIEndPoint() }serviceOrderManagement";

                            CreateOrderRequest request = new CreateOrderRequest
                            {
                                header = new smorderheader
                                {
                                    channelApplication = new smchannelapplication
                                    {
                                        channelId = ((int)ChannelType.SMS).ToString()
                                    }
                                },
                                orderLine = new smorderline[]
                                {
                                new smorderline {
                                    msisdn = opTurkcellMessages.ReceiverMsIsdn,
                                    orderLineItem = new smorderlineitem []
                                    {
                                        new smorderlineitem
                                        {
                                            offerId = HelperFunction.GetOfferId(),
                                            campaignId = HelperFunction.GetCampaignId(),
                                            action = serviceAction.ToString(),
                                            attribute = new smattribute[]
                                            {
                                                new smattribute
                                                {
                                                    key = "INITIAL_START_DATE",
                                                    value = DateTime.Now.ToString("yyyyMMddHHmmss")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            };
                            Task.Run(() => service.GetServiceAsync(JObject.FromObject(request).ToString()));
                        }
                        else
                        {
                            ServiceLogId = ServiceLogger.Log((int)ServiceType.ReceiveMO, (int)LogTypes.Error, message.MP_STRING_CONTENT, message.RECEIVER_MSISDN, true);
                        }
                        //opTurkcellMessages.ServiceLogId = ServiceLogId;
                        //_opTurkcellMessages.Add(opTurkcellMessages);

                        _tSOresult.statusCode = "0";
                        _tSOresult.errorCode = "0";
                        _tSOresult.errorDescription = "";
                    }
                }
                else
                {
                    _tSOresult.statusCode = "1";
                    _tSOresult.errorCode = "800";
                    _tSOresult.errorDescription = "ReceiveMOServiceRequest is null.";
                }

                return new ReceiveMOServiceResponse { TSOresult = _tSOresult };
            }
            catch (Exception)
            {
                _tSOresult.statusCode = "1";
                _tSOresult.errorCode = "801";
                _tSOresult.errorDescription = "ReceiveMOServiceRequest is null.";

                return new ReceiveMOServiceResponse { TSOresult = _tSOresult };
            }
        }
    }
}