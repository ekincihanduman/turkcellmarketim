﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Services.Description;
using System.ServiceModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Net.Security;
using TurkcellOutboundService.DataSource.Repositories.EntityModel;
using TurkcellOutboundService.DataSource.Repositories;
using TurkcellOutboundService.DataSource.EF;
using TurkcellOutboundService.Sources;

namespace Turkcellmarketim.ProvisionService
{
    /// <summary>
    /// Summary description for Provision
    /// </summary>
    //[MessageContract(IsWrapped = false)]

    [WebService(Namespace = "http://www.turkcell.com/spgw/PartnerServiceProvisioning", Description = "PartnerServiceProvisioning",   Name = "PartnerServiceProvisioning")]
    //[SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.SoapAction, Use = SoapBindingUse.Literal)]
    [WebServiceBinding(Name = "PartnerServiceProvisioning_Binding",   Namespace = "http://www.turkcell.com/spgw/PartnerServiceProvisioning",
        ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2612.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [WebServiceBindingAttribute(ConformsTo = WsiProfiles.BasicProfile1_1, Name = "PartnerServiceProvisioning")]
    [MessageContract(WrapperName = "partnerServiceProvisioning", IsWrapped = false)]
    [System.ComponentModel.ToolboxItem(false)]
    public class provision : WebService
    {
        public static ServiceLogger ServiceLogger = ServiceLogger.sLog();
        private readonly ApplicationRepositroy<ServiceProvisionLog> _serviceProvisionLog;
        public provision()
        {
            _serviceProvisionLog = _serviceProvisionLog ?? new ApplicationRepositroy<ServiceProvisionLog>();
        }

        [WebMethod]
        [SoapDocumentMethod(Action = "http://sdp.turkcell.com.tr/services/action/PartnerServiceProvisioning/partnerServiceProvisioning",
            RequestElementName = "PartnerServiceProvisioningRequest",  RequestNamespace = "http://www.turkcell.com/spgw/partnerserviceprovisioning/PartnerServiceProvisioningTypes",
            ResponseElementName = "PartnerServiceProvisioningResponse", ResponseNamespace = "http://partnerprovisioningservice.turkcell.services.ws.fourplay.com.tr",
            Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Bare, 
            Binding = "PartnerServiceProvisioning_Binding")]
        [FaultContractAttribute(
        typeof(PartnerServiceProvisioningRequest), Name = "fault1",
        ProtectionLevel = ProtectionLevel.EncryptAndSign)]

        [return: XmlElement("PartnerServiceProvisioningResponse")]
        [return: MessageParameter(Name = "PartnerServiceProvisioning")]
        public PartnerServiceProvisioningResponse PartnerServiceProvisioning(PartnerServiceProvisioningRequest PartnerServiceProvisioningRequest)
        {
            try
            {
                ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, PartnerServiceProvisioningRequest.action + " start", PartnerServiceProvisioningRequest.msisdn, true);
                //Servis sonucu, 0 : başarılı, 1: hatalı

                PartnerServiceProvisioningResponse PartnerServiceProvisioningResponse = new PartnerServiceProvisioningResponse();

                if (PartnerServiceProvisioningRequest != null)
                {
                    var provisionModel = _serviceProvisionLog.GetList(x => x.Msisdn == PartnerServiceProvisioningRequest.msisdn).FirstOrDefault();

                    ServiceProvisionLog Model = provisionModel ?? new ServiceProvisionLog();

                    Model.Action = PartnerServiceProvisioningRequest.action ?? "";
                    Model.Msisdn = PartnerServiceProvisioningRequest.msisdn ?? "";
                    Model.OfferId = PartnerServiceProvisioningRequest.offerId ?? "";
                    Model.ProductId = PartnerServiceProvisioningRequest.productId ?? "";
                    Model.ServiceVariantId = PartnerServiceProvisioningRequest.serviceVariantId ?? "";
                    Model.TransactionId = PartnerServiceProvisioningRequest.transactionId ?? "";
                    Model.ProvisionTime = DateTime.UtcNow;

                    if (provisionModel != null)
                        _serviceProvisionLog.Update(Model);
                    else
                        _serviceProvisionLog.Add(Model);

                    var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Ok, PartnerServiceProvisioningRequest.action, PartnerServiceProvisioningRequest.msisdn, true);

                    Model.ServiceLogId = ServiceLogId;

                    _serviceProvisionLog.Update(Model);

                    PartnerServiceProvisioningResponse.statusCode = "0";

                    return PartnerServiceProvisioningResponse;
                }

                PartnerServiceProvisioningResponse.statusCode = "1";
                PartnerServiceProvisioningResponse.errorCode = "900";
                PartnerServiceProvisioningResponse.errorDescription = "PartnerServiceProvisioningRequest is null.";

                return PartnerServiceProvisioningResponse;
            }
            catch (SoapException ex)
            {
                var ServiceLogId = ServiceLogger.Log((int)ServiceType.Provision, (int)LogTypes.Error, ex.Message, PartnerServiceProvisioningRequest.msisdn, false);

                //Servis sonucu, 0 : başarılı, 1: hatalı
                PartnerServiceProvisioningResponse response = new PartnerServiceProvisioningResponse
                {
                    statusCode = "1",
                    errorCode = "901",
                    errorDescription = "PartnerServiceProvisioningRequest failed. "  
                };
                return response;
            }
        }
    }
}