﻿using System;
using System.Collections.Generic;
using System.Text;
using TurkcellMarketim.DataAccess.Abstract;
using TurkcellMarketim.DataAccess.Concrete.EntityFramework;

namespace TurkcellMarketim.Business.EF.Abstract
{
    public interface IMarketimContext
    {
        EfLogTypes EfLogTypes(EfLogTypes efLogTypes);
        EfOpTurkcellMessageParameters EfOpTurkcellMessageParameters(EfOpTurkcellMessageParameters efOpTurkcellMessageParameters);
        EfOpTurkcellMessages EfOpTurkcellMessages(EfOpTurkcellMessages efOpTurkcellMessages);
        EfServiceProvisionLog EfServiceProvisionLog(EfServiceProvisionLog efServiceProvisionLog);
        EfServices EfServices(EfServices efServices);
        EfServiceSentSmsLog EfServiceSentSmsLog(EfServiceSentSmsLog efServiceSentSmsLog);
    }
}
